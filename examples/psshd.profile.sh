# Persistent, daemonised SSH tunnel to your favourite VPS, setting up a SOCKS
# proxy on port 8001, and using port 9010 for management. Goes nicely in a
# ~/.profile or ~/.bash_profile script.
psshd 9010 -fN -D 8001 myvps
