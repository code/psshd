psshd
=====

Persistent, daemonized SSH tunnels. Requires [`start-stop-daemon(8)`][sdm] and
[`autossh(1)`][asm] commands. Designed to be called for login shells, in
`~/.profile` or `~/.bash_profile`.

    $ psshd 9010 -fN -D 8001 myvps

License
-------

Copyright (c) [Tom Ryder][atr]. Distributed under an [MIT License][mit].

[sdm]: http://man.he.net/man8/start-stop-daemon
[asm]: http://linux.die.net/man/1/autossh
[atr]: https://sanctum.geek.nz/
[mit]: https://www.opensource.org/licenses/MIT
